
<?php
$list = "";
$randomnum = 0;
$linestep = 0;
$binstep = 0;

if (isset($_POST)&&isset($_POST['random'])){
    $origin = array(1,3,7,8,9,11,12,17,19,20,23,25);
    $randomnum = rand(1,25);
    $linearres =  returnarray($origin,$randomnum);
    foreach ($linearres as $key => $value) {
        $list.=$value." ";
    }
    $linestep = linear($origin,$randomnum);
    $binstep = binary($origin,$randomnum);
    
}
function returnarray($inputarr,$num){
    $step=0;
    $first = $inputarr[0];
    $arr1 = array();
    $arr2 = array();
    if ($first>$num) return array_merge(array($num),$inputarr);
    while ($step < count($inputarr)){
       
       if ($inputarr[$step]>$num){
        $arr1 = array_slice($inputarr,0,$step);
        $arr1[]=$num;
        $arr2 = array_slice($inputarr,$step);
        break;
       }
       $step++;
    };
    
    return array_merge($inputarr,array($num));
}
function linear($inputarr,$num){
    $step=0;
    $first = $inputarr[0];
    $arr1 = array();
    $arr2 = array();
    if ($first>$num) return $step;
    while ($step < count($inputarr)){
       
       if ($inputarr[$step]>$num){
        return $step;        
       }
       $step++;
    };
    return $step;
}
function binary($inputarr,$num){
    $step=0;
    $st=1;
    $en = count($inputarr);
    $arr1 = array();
    $arr2 = array();
    if ($inputarr[0]>$num) return $step;
    while ($st <= $en) {
        $step++;
        $mid = $st+intval(($en-$st)/2);
        if ($inputarr[$mid]==$num){
            
            return $step;
        }else if($inputarr[$mid]<$num){
            $st = $mid+1;
        }else  $en = $mid-1;
    }
    return $step;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
        <label for="">Origin :  1 3 7 8 9 11 12 17 19 20 23 25</label>
        </br>
        <label for="">Number generated : <?=$randomnum?></label>
        </br>
        <label for="">List result : <?=$list?> </label>
        </br>
        <label for="">Linear step: <?=$linestep?></label>
        </br>
        <label for="">Binary step: <?=$binstep?> </label>
        </br>
        <input type="submit" name="random" value="random">
    </form>
    
</body>
</html>